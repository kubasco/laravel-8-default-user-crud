<?php

use App\Http\Controllers\Auth\ConfirmPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
Route::get('password/confirm', [ConfirmPasswordController::class, 'showConfirmForm'])->name('password.confirm');
Route::post('password/confirm', [ConfirmPasswordController::class, 'confirm']);
Route::get('email/verify', [VerificationController::class, 'show'])->name('verification.notice');
Route::get('email/verify/{id}/{hash}', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');

Route::middleware(
    [
        'auth:web'
    ]
)->group(
    static function () {
        Route::get('home', [HomeController::class, 'index'])->name('home');

        Route::prefix('users')->as('users.')->group(
            static function () {
                Route::get('', [UserController::class, 'index'])
                    ->name('index');
                Route::get('/search', [UserController::class, 'search'])
                    ->name('search');
                Route::get('/create', [UserController::class, 'create'])
                    ->name('create');
                Route::post('/store', [UserController::class, 'store'])
                    ->name('store');
                Route::get('/{user}/edit', [UserController::class, 'edit'])
                    ->name('edit')
                    ->where('user', '[0-9]+');
                Route::put('{user}/update', [UserController::class, 'update'])
                    ->name('update')
                    ->where('user', '[0-9]+');
                Route::get('/{user}/delete', [UserController::class, 'delete'])
                    ->name('delete')
                    ->where('user', '[0-9]+');
            }
        );
    });