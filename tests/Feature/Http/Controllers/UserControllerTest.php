<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testIndex(): void
    {
        $users = User::factory()->times(5)->create();

        $response = $this->actingAs($users->first())
            ->get(route('users.index'));

        $response
            ->assertOk()
            ->assertSeeInOrder($users->pluck(User::FIELD_EMAIL)->all());
    }

    public function testSortedIndex(): void
    {
        $users = User::factory()->times(5)->create();

        $this
            ->actingAs($users->first())
            ->get(
                route(
                    'users.index',
                    [
                        'orderBy' => User::FIELD_ID,
                        'direction' => 'desc'
                    ]
                )
            )
            ->assertOk()
            ->assertSeeInOrder($users->sortBy(User::FIELD_ID)->pluck(User::FIELD_ID)->all());
    }

    public function testSearchField(): void
    {
        $users = User::factory()->times(5)->create();

        $this
            ->actingAs($users->first())
            ->get(route('users.index'))
            ->assertOk()
            ->assertSee('type="search"', false)
            ->assertSeeText(__('dashboard.users.search'));
    }

    public function testSearch(): void
    {
        $users = User::factory()->times(5)->create();

        $this
            ->actingAs($users->first())
            ->get(route('users.search', [User::FIELD_EMAIL => $users->last()->getEmail()]))
            ->assertOk()
            ->assertSeeText($users->last()->getEmail())
            ->assertSeeText(__('dashboard.results_for'), false);
    }

    public function testCreateUser(): void
    {
        $user = User::factory()->create();

        $this->assertDatabaseHas(
            User::TABLE,
            [
                User::FIELD_EMAIL => $user->email
            ]
        );
    }

    public function testLoggedInUserCreatesAnotherUser(): void
    {
        $user = User::factory()->create();
        $newUserEmail = $this->faker->unique()->safeEmail;
        $response = $this
            ->actingAs($user)
            ->post(route('users.store'), [
                User::FIELD_NAME => $this->faker->name,
                User::FIELD_EMAIL => $newUserEmail,
                User::FIELD_PASSWORD => 'password'
            ]);

        $response
            ->assertRedirect(route('users.index'));

        $this->assertDatabaseHas(
            User::TABLE,
            [
                User::FIELD_EMAIL => $newUserEmail
            ]
        );
    }
}