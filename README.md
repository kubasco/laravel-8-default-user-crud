## About this Project

This project is written for show Laravel working with simple User CRUD.
For simplify we do not use NPM and any other for frontend things.
We focus on backend.

## How to start ?

- prepare database table
- copy .env.example to .env
- set proper databse connetion in .env
- run console
- composer update
- php artisan migrate:fresh --seed

## Default develop credentials to Admin Panel

- admin@example.com / password

## Test

Yes. Feature tests are written with use of PHPUnit.

## So what next?

This is simple "starter" for boost up Your development.
Here You are simple User's CRUD and some nice Admin Panel connected.
Everything You can easily build with views in blade.

## What plugins are included for views ?
https://getbootstrap.com/docs
https://jqueryui.com
https://fontawesome.com
https://select2.org
https://www.npmjs.com/package/ekko-lightbox