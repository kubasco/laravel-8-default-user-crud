@extends('layouts.app')

@section('content')

    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 mt-3 mb-3">
                    <div class="block">
                        <h1>{{ __('dashboard.home.title') }} : {{ config('app.name') }}</h1>
                    </div>
                    <hr>
                </div>

                <div class="col-lg-12">
                    <div class="block">
                        <a class="btn btn-outline-info"
                           href="{{ route('users.index') }}">
                            {{ __('dashboard.users.title') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
