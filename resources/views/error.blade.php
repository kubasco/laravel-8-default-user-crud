@extends('layouts.app')

@section('content')

    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 badge-danger p-5">
                    <h3>{{ __('dashboard.errors_occurs') }}</h3>
                    <div>
                        @if ($errors->any())
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
