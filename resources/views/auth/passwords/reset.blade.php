<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    @include('layouts.styles.header_styles')
    @include('layouts.scripts.header_scripts')
</head>
<body class="bg-primary">
@include('layouts.sections.success')
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main class="fadeIn">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="bg-white shadow-lg border-0 mt-5 p-2">
                            <div class="card-header text-center bg-white p-3">
                            {{ config('app.name') }}
                            </div>
                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="user-block block text-center badge-danger">
                                        <ul class="p-2">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('password.update') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputEmailAddress">
                                            {{ __('dashboard.email') }}
                                        </label>
                                        <input type="hidden" name="token" value="{{ $token ?? old('token') }}">
                                        <input class="form-control @error('email') is-invalid @enderror"
                                               name="email"
                                               id="inputEmailAddress"
                                               value="{{ $email ?? old('email') }}"
                                               type="email"
                                               placeholder="{{ __('dashboard.email_placeholder') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputPassword">
                                            {{ __('dashboard.password') }}
                                        </label>
                                        <input class="form-control @error('email') is-invalid @enderror"
                                               name="password"
                                               id="inputPassword"
                                               value="{{ old('password') }}"
                                               type="password"
                                               placeholder="{{ __('dashboard.password_placeholder') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputPassword">
                                            {{ __('dashboard.password_confirmation') }}
                                        </label>
                                        <input class="form-control @error('email') is-invalid @enderror"
                                               name="password_confirmation"
                                               id="inputPassword"
                                               value="{{ old('password_confirmation') }}"
                                               type="password"
                                               placeholder="{{ __('dashboard.password_confirmation_placeholder') }}">
                                    </div>
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                        <button type="submit" class="btn btn-block btn-danger btn-lg">{{ __('dashboard.password_reset_send') }}</button>
                                    </div>
                                    <a class="small text-primary"
                                       href="{{ route('login') }}"> {{ __('dashboard.back_to') }} {{ __('dashboard.login') }}
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@include('layouts.scripts.footer_scripts')
</body>
</html>