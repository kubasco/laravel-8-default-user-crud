@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 mt-3 mb-3">
                    <div class="block">
                        <h1>{{ __('dashboard.users.add') }}</h1>
                    </div>
                    <hr>
                </div>

                <div class="col-lg-10">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('users.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.users.name') }}</label>
                                <div class="col-sm-9">
                                    <input name="name"
                                           type="text"
                                           class="form-control{!! (isset($errors) && $errors->first('name')) ? ' is-invalid' : '' !!}"
                                           value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.users.email') }}</label>
                                <div class="col-sm-9">
                                    <input name="email"
                                           type="text"
                                           class="form-control{!! (isset($errors) && $errors->first('email')) ? ' is-invalid' : '' !!}"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.users.password') }}</label>
                                <div class="col-sm-9">
                                    <input name="password"
                                           type="password"
                                           class="form-control{!! (isset($errors) && $errors->first('password')) ? ' is-invalid' : '' !!}"
                                           value="{{ old('password') }}">
                                </div>
                            </div>

                                <div class="col-sm-9 offset-3 mt-2 mb-4">
                                    <button type="submit"
                                            class="btn btn-lg btn-outline-success btn-block">
                                        {{ __('dashboard.save') }}
                                    </button>
                                </div>

                        </form>

                    </div>
                </div>

                <div class="col-lg-12">
                    <hr>
                    <div class="block">
                        <a class="btn btn-outline-danger"
                           href="{{ route('users.index') }}">
                            {{ __('dashboard.back_to') }} {{ __('dashboard.users.title') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
