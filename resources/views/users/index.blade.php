@extends('layouts.app')

@section('content')

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 mt-3 mb-3">
                    <div class="block">
                        <h1>{{ __('dashboard.users.title') }}
                            <a class="btn btn-outline-secondary btn-sm" href="{{ route('users.create') }}">
                                {{ __('dashboard.users.add') }}</a>
                        </h1>

                    </div>
                </div>

                <div class="col-lg-12 badge-light pt-2">
                    <div class="block">
                        <form action="{{route('users.search')}}">
                            @csrf
                            <div class="form-group row">
                                <span class="col-sm-2 mt-2">
                                    <label for="{{ \App\Models\User::FIELD_EMAIL }}"
                                            class="form-control-label">
                                        {{__('dashboard.users.search')}}
                                    </label>
                                </span>
                                <span class="col-sm-4 mt-2">
                                    <input class="form-control"
                                           type="search"
                                           id="{{ \App\Models\User::FIELD_EMAIL }}"
                                           name="{{ \App\Models\User::FIELD_EMAIL }}"
                                           placeholder="{{ __('dashboard.users.email_search_placeholder') }}"
                                           value="{{ $query ?? '' }}"/>
                                </span>
                                <span class="col-sm-4 mt-2">
                                    <input
                                            class="btn btn-outline-info"
                                            type="submit" value="{{__('dashboard.users.search')}}"
                                    />
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-12 mb-3 mt-4">
                    <div class="text-center">
                        {{ $users->links() }}
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="block">
                        @if(isset($query))
                            <p class="text-gray-light">
                                {{ __('dashboard.results_for') }} : {{ $query }}
                            </p>
                            <hr>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-sm text-gray-light">
                                <thead>
                                <tr>
                                    <th style="min-width: 50px;">
                                        @if(!isset($query))
                                            <span>
                                            <a href="{{route('users.index',
                                            [
                                            \App\Models\User::KEY_ORDER_BY => 'id',
                                            \App\Models\User::KEY_DIRECTION => 'asc'
                                            ])}}">
                                                <i class="fa fa-sort-numeric-up-alt"></i>
                                            </a>
                                        </span>
                                            <span>
                                            <a href="{{route('users.index',
                                            [
                                            \App\Models\User::KEY_ORDER_BY => 'id',
                                            \App\Models\User::KEY_DIRECTION => 'desc'
                                            ])}}">
                                                <i class="fa fa-sort-numeric-down-alt"></i>
                                            </a>
                                        </span>
                                            <br>
                                        @endif
                                        {{__('dashboard.id')}}
                                    </th>
                                    <th>{{ __('dashboard.users.name') }}</th>
                                    <th>{{ __('dashboard.users.email') }}</th>
                                    <th>{{ __('dashboard.users.action') }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <a href="{{ route('users.edit', $user->id) }}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a>
                                            <a href="{{ route('users.delete', $user->id) }}"
                                               data-toggle="tooltip" data-placement="left"
                                               title="{{ __('dashboard.users.delete') }}"
                                               onclick="return confirm('{{ __('dashboard.confirm_action') }}')"
                                            >
                                                <i class="fa fa-times text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mb-5">
                    <hr>
                    <div class="pt-2 text-center">
                        {{ $users->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
