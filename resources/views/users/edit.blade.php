@extends('layouts.app')

@section('content')

    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12 mt-3 mb-3">
                    <div class="block">
                        <h1>{{ __('dashboard.users.edit') }}</h1>
                    </div>
                    <hr>
                </div>

                <div class="col-lg-10">
                    <div class="user-block block text-center">

                        <form method="POST" action="{{ route('users.update', $user->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">{{ __('dashboard.users.name') }}</label>
                                <div class="col-sm-9">
                                    <input name="name"
                                           type="text"
                                           class="form-control{!! (isset($errors) && $errors->first('name')) ? ' is-invalid' : '' !!}"
                                           value="{{ old('name') ?: $user->name }}">
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="col-sm-9 offset-3 mt-2 mb-4">
                                <button type="submit"
                                        class="btn btn-lg btn-outline-success btn-block">
                                    {{ __('dashboard.save') }}
                                </button>
                            </div>

                        </form>

                    </div>
                </div>

                <div class="col-lg-12">
                    <hr>
                    <div class="block">
                        <a class="btn btn-outline-danger"
                           href="{{ route('users.index') }}">
                            {{ __('dashboard.back_to') }} {{ __('dashboard.users.title') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
