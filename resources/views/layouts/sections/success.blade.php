@if (isset($success) || session('success') || session('status') || (isset($errors) && $errors->any()) || (string)request()->old('success') !== '')
    @if ((isset($success) && $success === true) || (session()->has('success') && session()->get('success') === true))
        <div class="fadeInDown col-12 success_modal bg-success p-4" style="position: fixed;z-index:100000000;">
            <div class="user-block block text-center badge-success">
                <h3>{{ __('dashboard.success') }}</h3>
            </div>
        </div>
    @elseif((string)request()->old('success') !== '')
        <div class="fadeInDown col-12 success_modal bg-success p-4" style="position: fixed;z-index:100000000;">
            <div class="user-block block text-center badge-success">
                <h3>{{ __('dashboard.success') }}</h3>
                <p>{{ request()->old('success') }}</p>
            </div>
        </div>
    @elseif(session('status') || session('success'))
        <div class="fadeInDown col-12 success_modal bg-success p-4" style="position: fixed;z-index:100000000;">
            <div class="user-block block text-center badge-success">
                <h3>{{ __('dashboard.success') }}</h3>
            </div>
        </div>
    @elseif((isset($success) && $success === false) || $errors->any())
        <div class="fadeInDown col-12 success_modal bg-danger p-4" style="position: fixed;z-index:100000000;">
            <div class="user-block block text-center badge-danger">
                @if(isset($errors))
                    <h3>
                        {{ __('dashboard.errors') }}
                    </h3>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @else
                    <h3>{{ __('dashboard.errors') }}</h3>
                @endif
            </div>
        </div>
    @else
        <div class="fadeInDown col-12 success_modal bg-info p-4" style="position: fixed;z-index:100000000;">
            <div class="user-block block text-center badge-info">
                <h3>{{ $success }}</h3>
            </div>
        </div>
    @endif
    <script>
        window.setTimeout(function () {
            $(".success_modal").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 1500);
    </script>
@endif
