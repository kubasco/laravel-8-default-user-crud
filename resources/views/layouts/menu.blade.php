<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <div class="sb-sidenav-menu-heading">{{ __('dashboard.menu') }}</div>
            <a class="nav-link" href="{{ route('users.index') }}">
                <div class="sb-nav-link-icon">
                    <i class="fa fa-user"></i>
                </div>
                {{ __('dashboard.users.title') }}
            </a>
        </div>
    </div>
</nav>
