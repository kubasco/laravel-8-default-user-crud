<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    @include('layouts.styles.header_styles')
    @include('layouts.scripts.header_scripts')
</head>
<body class="sb-nav-fixed" onbeforeunload="return showLoader()">
    @include('layouts.sections.success')
    @include('layouts.menu_up')
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            @include('layouts.menu')
        </div>
        <div id="layoutSidenav_content">
            <div id="loader" class="badge m-3 d-none">
                <img decoding="async" src="{{ asset('img/loader.gif') }}" alt="loader">
            </div>
            <main id="data_results">
                @yield('content')
            </main>
        </div>
    </div>
    @include('layouts.scripts.footer_scripts')
</body>
</html>
