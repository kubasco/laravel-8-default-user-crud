<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'errors' => 'Znaleziono problemy',
    'email' => 'Email',
    'email_placeholder' => 'Podaj Email',
    'password' => 'Hasło',
    'password_placeholder' => 'Podaj hasło',
    'password_confirmation' => 'Potwórz Hasło',
    'password_confirmation_placeholder' => 'Podaj hasło jeszcze raz',
    'sign_in' => 'Zaloguj się',
    'password_reset' => 'Reset hasła',
    'back_to' => 'Wróć do :',
    'login' => 'Logowanie',
    'password_reset_send' => 'Resetuj hasło',
    'logout' => 'Wyloguj',
    'menu' => 'Menu',
    'menu_admin' => 'Menu Administratora',
    'success' => 'Operacja powiodła się',
    'id' => 'ID',
    'action' => 'Akcja',
    'save' => 'Zapisz',
    'confirm_action' => 'Czy na pewno chcesz wykonać tę akcję?',

    'home' => [
        'title' => 'System',
    ],

    'users' => [
        'add' => 'Dodaj użytkownika',
        'edit' => 'Edytuj użytkownika',
        'delete' => 'Usuń użytkownika',
        'title' => 'Użytkownicy',
        'name' => 'Nazwa',
        'email' => 'Email',
        'email_search_placeholder' => 'Wyszukaj po mailu',
        'password' => 'Hasło',
        'action' => 'Akcja',
        'search' => 'Szukaj użytkownika',
    ],
];
