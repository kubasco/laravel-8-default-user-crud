<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    private const DEFAULT_RESULTS_PER_PAGE = 20;

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $users = User::query()
            ->orderBy(
                $request->get(User::KEY_ORDER_BY) ?? User::FIELD_ID,
                $request->get(User::KEY_DIRECTION) ?? 'asc'
            )
            ->simplePaginate(self::DEFAULT_RESULTS_PER_PAGE);

        return view('users.index', ['users' => $users]);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function search(Request $request): View
    {
        $query = $request->get(User::FIELD_EMAIL);
        $users = User::query()
            ->where(User::FIELD_EMAIL, 'LIKE', "%{$query}%")
            ->simplePaginate(self::DEFAULT_RESULTS_PER_PAGE);

        return view(
            'users.index',
            [
                'users' => $users,
                'query' => $query
            ]
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('users.create');
    }

    /**
     * @param UserStoreRequest $request
     * @return RedirectResponse
     */
    public function store(UserStoreRequest $request): RedirectResponse
    {
        $user = User::create($request->validated());
        event(new Registered($user));

        return redirect()->route('users.index')->withSuccess(__('dashboard.success'));
    }

    /**
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }

    /**
     * @param UserUpdateRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UserUpdateRequest $request, User $user): RedirectResponse
    {
        $user->update($request->validated());

        return redirect()->route('users.index')->withSuccess(__('dashboard.success'));
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function delete(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()->route('users.index')->withSuccess(__('dashboard.success'));
    }

}
