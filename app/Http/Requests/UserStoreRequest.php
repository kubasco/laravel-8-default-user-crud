<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            User::FIELD_NAME => 'required|string|min:2|max:128',
            User::FIELD_EMAIL => 'required|email|unique:users,email',
            User::FIELD_PASSWORD => 'required|string|min:8|max:256',
        ];
    }
}
